﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progmilestone3_9858737
{
    class Program
    {
        static void Main(string[] args)
        {
            int menuScreen;
            string pizzaflavour;
            int i;
            int x;
            var flavour = new List<string> { "Pepperoni", "Vegertarian", "Ultimate Dab", "Mystery Meat", "German Youth", "Harambe Special" };
            var order = new List<Tuple<string, string, double>>();
            double cost = 0;
            string owing;
            double payment = 0;

            List<Tuple<string, double>> pizza = new List<Tuple<string, double>>();
            pizza.Add(Tuple.Create("Kids", 6.21));
            pizza.Add(Tuple.Create("Classic", 11.47));
            pizza.Add(Tuple.Create("Large", 52.31));

            List<Tuple<string, double>> drink = new List<Tuple<string, double>>();
            drink.Add(Tuple.Create("Coffee", 4.57));
            drink.Add(Tuple.Create("Tea", 4.57));
            drink.Add(Tuple.Create("Jesus Juice", 20.00));
            drink.Add(Tuple.Create("Golden Shower", 150.00));

            welcomeScreen:
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            Console.WriteLine("Welcome to super mega pizza dragon palace");
            Console.WriteLine(" press any key to continue");
            Console.ReadLine();
            Customer.custDetails();

        menuScreen:

            Console.Clear();
            Orderdetails.display();
            Console.WriteLine();
            Console.WriteLine("Menu Select:");
            Console.WriteLine("1. Pizza");
            Console.WriteLine("2. Drinks");
            Console.WriteLine("3. Place Order");
            Console.WriteLine();


            if (int.TryParse(Console.ReadLine(), out x))
            {
                switch (x)
                {
                    case 1:
                    Pizzastart:
                        Console.Clear();
                        Console.WriteLine();
                        Console.WriteLine("Current Order:");
                        Orderdetails.display();
                        for (i = 0; i < flavour.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {flavour[i]}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menuScreen;
                            }
                            else if (x > i + 1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine();
                                Console.WriteLine("Error. dont be a dick.");
                                Console.ReadLine();
                                Console.ForegroundColor = ConsoleColor.Green;
                                goto Pizzastart;
                            }
                            else
                            {
                                pizzaflavour = flavour[x - 1];
                                Console.Clear();
                                Orderdetails.display();
                                for (i = 0; i < pizza.Count; i++)
                                {
                                    Console.WriteLine($"{i + 1}. {pizza[i].Item1}");
                                }
                                Console.WriteLine();
                                Console.WriteLine($"{i + 1}. Return to menu");

                                if (int.TryParse(Console.ReadLine(), out x))
                                {
                                    if (x == i + 1)
                                    {
                                        goto menuScreen;
                                    }
                                    else if (x > i + 1)
                                    {
                                        Console.WriteLine();
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Error. dont be a dick.");
                                        Console.ReadLine();
                                        Console.ForegroundColor = ConsoleColor.Green;
                                        goto Pizzastart;
                                    }
                                    else
                                    {
                                        order.Add(Tuple.Create(pizzaflavour, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                        Orderdetails.order.Add(Tuple.Create(pizzaflavour, pizza[x - 1].Item1, pizza[x - 1].Item2));
                                    }
                                    goto Pizzastart;
                                }
                                else
                                {
                                    Console.WriteLine();
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("Error. dont be a dick.");
                                    Console.ReadLine();
                                    Console.ForegroundColor = ConsoleColor.Green;
                                    goto Pizzastart;
                                }
                            }

                        }
                        else
                        {
                            Console.WriteLine();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Error. dont be a dick.");
                            Console.ReadLine();
                            Console.ForegroundColor = ConsoleColor.Green;
                            goto Pizzastart;
                        }


                    case 2:
                    Drinkstart:

                        Console.Clear();

                        Orderdetails.display();
                        for (i = 0; i < drink.Count; i++)
                        {
                            Console.WriteLine($"{i + 1}. {drink[i].Item1}");
                        }
                        Console.WriteLine();
                        Console.WriteLine($"{i + 1}. Return to menu");

                        if (int.TryParse(Console.ReadLine(), out x))
                        {
                            if (x == i + 1)
                            {
                                goto menuScreen;
                            }
                            else if (x > i + 1)
                            {
                                Console.WriteLine();
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Error. dont be a dick.");
                                Console.ReadLine();
                                Console.ForegroundColor = ConsoleColor.Green;
                                goto Drinkstart;
                            }
                            else
                            {
                                order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                                Orderdetails.order.Add(Tuple.Create("Drink     ", drink[x - 1].Item1, drink[x - 1].Item2));
                            }
                            goto Drinkstart;
                        }
                        else
                        {
                            Console.WriteLine();
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Error. dont be a dick..");
                            Console.ReadLine();
                            Console.ForegroundColor = ConsoleColor.Green;
                            goto Drinkstart;
                        }

                    case 3:
                    Confirm:

                            Console.Clear();
                            Orderdetails.display();
                            Console.WriteLine("We good son?");
                            Console.WriteLine("1. Yes");
                            Console.WriteLine("2. No");
                            if (int.TryParse(Console.ReadLine(), out x))
                            {
                                switch (x)
                                {
                                    case 1:
                                        Console.Clear();
                                        for (i = 0; i < order.Count; i++)
                                        {
                                            owing = String.Format("{0:C}", order[i].Item3);
                                            Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                                            cost = cost + order[i].Item3;
                                        }
                                        owing = String.Format("{0:C}", cost);
                                        Console.WriteLine();
                                        Console.WriteLine($"cost:   ${cost}");
                                        Console.WriteLine();

                                        Console.WriteLine(" How much cash have you got?");
                                        payment = double.Parse(Console.ReadLine());

                                        if (payment > cost)
                                        {
                                            payment = payment - cost;
                                            Console.WriteLine($"Thanks. here is your change ${payment}");
                                        Console.ReadLine();
                                        }
                                        else if (payment == cost)
                                        {
                                            Console.WriteLine("What? No tip?");
                                        Console.ReadLine();
                                        }
                                        else if (payment < cost)
                                        {
                                            cost = cost - payment;
                                            Console.WriteLine("You broke fool");
                                        Console.ReadLine();
                                        }
                                        Environment.Exit(0);
                                        break;

                                    case 2:
                                        goto menuScreen;
                                }
                            }
                        break;

                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error. dont be a dick.");
                        Console.ReadLine();
                        Console.ForegroundColor = ConsoleColor.Green;
                        goto menuScreen;




                }
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error. dont be a dick.");
                Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Green;
                goto menuScreen;
            }
            Orderdetails.display();
        }
    }

    public static class Orderdetails
    {

        public static List<Tuple<string, string, double>> order = new List<Tuple<string, string, double>>();

        public static void display()
        {
            int i;
            double cost = 0;
            string owing;


            Console.WriteLine($"Name: {Customer.name}");
            Console.WriteLine($"Address: {Customer.displayaddy}");

            Console.WriteLine("Order Summary:");

            for (i = 0; i < order.Count; i++)
            {
                owing = String.Format("{0:C}", order[i].Item3);
                Console.WriteLine($"{order[i].Item1}      {order[i].Item2}      {owing}");
                cost = cost + order[i].Item3;
            }
            owing = String.Format("{0:C}", cost);
            Console.WriteLine();
            Console.WriteLine($"                cost:              ${cost}");

        }

    }


    public static class Customer
    {
        public static string name;
        public static string displayaddy;


        public static void custDetails()
        {
            int x;
        Start:
            Console.Clear();

            Console.WriteLine();
            Console.WriteLine("What is the name for the order?");
            Console.WriteLine();

            name = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("And an address for delivery.");
            Console.WriteLine();

            displayaddy = Console.ReadLine();
           
        }

    }



}
